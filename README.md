#### Starting with XPM
You need to know that XPM is written in shell, and is a *binary* package manager, not a source package manager.
It requires: bash/zsh/any POSIX compliant shell, tar, gnu core utils/busybox
#### Copyright
Linux® is the registered trademark of Linus Torvalds in the U.S. and other countries.